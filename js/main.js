window.addEventListener("load", addListeners, false);

function addListeners(){
    addListenersToIcons();
    addListenerToExit();
}

//Añade manejador de eventos a los iconos del menu de circusntancias
function addListenersToIcons(){
    var icons = document.querySelectorAll("[data-target]");
    for(var i=0; i<icons.length; i++){
        icons[i].addEventListener("click", hacerVisibleCircustancia, false);
    }
}

function hacerVisibleCircustancia(evento){
    //Primero recuperamos el elemento "icon" y de este usamos el atributo
    //data-target, cuyo valor es el identificador de la circunstancia que se 
    //va a hacer visible.
    var target = evento.currentTarget.dataset.target;
    var circunstance = document.getElementById(target);
    toggleAnimacionCircustancia(circunstance);
} 

function toggleAnimacionCircustancia(circunstance){
    toggleVisibilityIcons();
    toggleEfectoBlind(circunstance);
    toggleEfectoFadein(circunstance);
}

//Funcion para evitar que los iconos del menu circustancias se sigan viendo 
//una vez se han presionado sobre ellos.
function toggleVisibilityIcons(){
    var icons = document.querySelectorAll("[data-target]");
    for(var i=0; i<icons.length; i++){
        icons[i].classList.toggle("visible");
    }
}

//Esta funcion gestiona que el contenedor "circustance" aparezca con un efecto
//ventana
function toggleEfectoBlind(circunstance){
    circunstance.classList.toggle("blindEffect");
}

//Esta funcion gestiona que la imagen y texto del contenedor aparezcan poco a 
//poco
function toggleEfectoFadein(circunstance){
    var transparentes = circunstance.getElementsByClassName("fadeinTarget");
    for(var i=0; i<transparentes.length; i++){
        transparentes[i].classList.toggle("fadein");
    }
}

//Añade manejador de eventos a la "X" de salida
function addListenerToExit(){
    var exitButtons = document.querySelectorAll(".circumstance button");
    for(var i=0; i<exitButtons.length; i++){
        exitButtons[i].addEventListener("click", hacerInvisibleCircustancia, false);
    }
}

function hacerInvisibleCircustancia(evento){
    //Accedemos al boton "X" y de el a su nodo padre "circunstance"
    var circustance = evento.target.parentNode;
    toggleAnimacionCircustancia(circustance);
}