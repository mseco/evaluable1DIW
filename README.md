# README / LEEME

*Evaluable:* 1

*Módulo:* Diseño de Interfaces WEB

*Ciclo Formativo:* Desarrollo de Aplicaciones WEB

*Objetivo:* Realizar actividad evaluable numero 1

## Author / Autor

Miguel Seco Pons 


## License / Licencia

Contenido de la página: 

José Miguel Marín, Departamento de Lengua Castellana del CEEDCV.


Imágenes:

WHALE, James (1931) Fotograma de la película Frankenstein.
	http://www.channelvideoone.com/2013/03/el-doctor-frankenstein1931-pelicula.html

Autor desconocido Grabado de la Villa Diodati.
	http://lasoga.org/las-sombras-villa-diodati/

ROTHWELL, Richard. (circa 1840) Retrato de Mari Shelley.
	https://es.wikipedia.org/wiki/Mary_Shelley

SHELLEY, Mary. (1831) Frankenstein o el moderno Prometeo. Cubierta interior
	https://es.wikipedia.org/wiki/Frankenstein_o_el_moderno_Prometeo#/media/File:Frankenstein.1831.inside-cover.jpg

Autor desconocido Volcán Monte Tambora.
	http://www.abc.es/cultura/20150826/abci-erupcion-volcan-oscurecio-mundo-201508251356.html

Google Maps.
	https://www.google.es/maps/@24.8512888,50.9927327,3z

Wikipedia.
	https://es.wikipedia.org/wiki/Año_sin_verano#/media/File:1816_summer.png

GALVANI, Luigi (1791) De viribus electricitatis in moto muscular commentarius.
	https://pixabay.com/es/fr%C3%ADo-termómetro-clima-159379/

papyrus (licencia "Labeled for reuse")
	https://pixabay.com/en/parchment-papyrus-dirty-old-dirt-435347/ 
